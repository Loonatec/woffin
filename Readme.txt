The Balloon Woffin (https://bitbucket.org/Loonatec/woffin/) by Balloon Ascent Technologies LLC (https://Loonatec.com)

This product has been designed by Balloon Ascent Technologies LLC. It has been licensed under the CERN Open Hardware License v1.2 (see included License.txt file).
All trademarks (Woffin, Loonatec) are owned by the original designer, Balloon Ascent Technologies LLC. Please respect the trademarks of others.

# Summary
The Balloon Woffin is a weigh-off inflation device for latex weather balloons. It is placed in the neck of the balloon and permits inflation through a short
length of latex tubing. Once the proper inflation is achieved, usually monitored by a weigh-off weight, the latex tubing is pinched shut. This seals the
balloon, permitting a quick launch.

# Change-log
Initial release 2019-07-11
